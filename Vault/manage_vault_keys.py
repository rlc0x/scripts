#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import re
import subprocess
import shlex
import argparse
from pprint import pprint
import requests
import hvac
from collections import defaultdict
from fs.tempfs import TempFS
import json

class ManageVaultSecrets(object):
    """This class provides method to manage Hashicorp Vault entries
    """

    def __init__(self, **kwargs):
        """Gather command line entries and such
        Args:
            hcv_url (str): URL to vault server
            hcv_ca_cert (str): Path to CA certificate bundle
            hcv_mount_point (str): The path the secret engine is mounted on
            hcv_path (str): Path to the secret(s)
            server (str): The name of a server object to check or create keys for
            users (list): List of names to create keys for, defaults to just root
        """
        allowed_params = ['hcv_url',
                'hcv_ca_cert',
                'hcv_mount_point',
                'hcv_path',
                'server',
                'users',
                ]
        params = locals()
        for key, val in params['kwargs'].items():
            if key not in allowed_params:
                raise TypeError(
                        'Got an unexpected keyword argument {} to method __init__'.format(key)
                        )
            params[key] = val
        del params['kwargs']

        if ('hcv_url' not in params) or (params['hcv_url'] is None):
            raise ValueError("Missing the required parameter `hcv_url` when calling `__init__`")
        else:
            self.hcv_url = params['hcv_url']

        if ('hcv_ca_cert' not in params) or (params['hcv_ca_cert'] is None):
            raise ValueError("Missing the required parameter `hcv_ca_cert` when calling `__init__`")
        else:
            self.hcv_ca_cert = params['hcv_ca_cert']

        if ('hcv_mount_point' not in params) or (params['hcv_mount_point'] is None):
            raise ValueError("Missing the required parameter `hcv_mount_point` when calling `__init__`")
        else:
            self.hcv_mount_point = params['hcv_mount_point']

        if ('hcv_path' not in params) or (params['hcv_path'] is None):
            raise ValueError("Missing the required parameter `hcv_path` when calling `__init__`")
        else:
            self.hcv_path = params['hcv_path']

        if ('server' not in params) or (params['server'] is None):
            self.server = 'all'
        else:
            self.server = params['server']

        if ('users' not in params) or (params['users'] is None):
            self.users = ['root',]
        else:
            self.users = params['users']

        self.top_dir = os.getcwd()

        # Initialize the server
        self.init_server()


    def init_server(self):
        """Initialize the connection to the Vault server
        Args:
            None
        Returns:
            Client object to allow working with the Vault server via API
        """
        self.client = hvac.Client(url=self.hcv_url, verify=self.hcv_ca_cert)
        if not self.client.is_authenticated():
            raise

    def list_servers(self):
        """Generate a list of servers stored in Vault
        Args:
            None
        Returns:
            List of servers
        """
        server_list = self.client.secrets.kv.v2.list_secrets(self.hcv_path,mount_point=self.hcv_mount_point)['data']['keys']
        return server_list

    def list_keys(self,srv):
        _path = f'servers/{srv}'

        ret = self.client.secrets.kv.v2.list_secrets(_path,mount_point=self.hcv_mount_point)['data']['keys']
        return(ret)

    def list_key(self,srv,key_name):
        """List public and private keys for a specific server
        """
        _path = f'servers/{srv}/{key_name}'
        ret = self.client.secrets.kv.v2.read_secret(_path,mount_point=self.hcv_mount_point)['data']['data'] #['ssh_public_key']
        return(ret)

    def list_public_keys(self):
        """List all of the public keys contained in vault
        """
        list_of_servers = self.list_servers()
        public_key_list = defaultdict(list)
        for srv in list_of_servers:
            list_of_keys = self.list_keys(srv)
            for key_name in list_of_keys:
                if key_name == 'host':
                    continue
                __path = f'servers/{srv}/{key_name}'

                public_key_list[key_name].append(self.client.secrets.kv.v2.read_secret(__path,mount_point=self.hcv_mount_point)['data']['data']['ssh_public_key'])
        return(public_key_list)

    def list_all_keys(self):
        """Generate a dictionary object for use in comparisons to local keys
        Structure:
            {'a-epiclnpus200': {'epicadm': {'ssh_private_key': 'KeyValue',
                                            'KeyName': 'KeyValue',
                                            'KeyName': 'KeyValue',
                                            }},
             'Servername': {
            {
                {ServerName:
                    { KeyName: KeyValue }
                },
                {ServerName:
                    { KeyName: KeyValue }
                },
            }
        """
        list_of_servers = self.list_servers()
        all_key_list = {}
        for srv in list_of_servers:
            srv = srv.rsplit('/')[0]
            all_key_list[srv] = {}
            list_of_keys = self.list_keys(srv)
            for key_name in list_of_keys:
                __path = f'{self.hcv_path}/{srv}/{key_name}'
                all_key_list[srv][key_name] = {}
                all_key_list[srv][key_name] = self.client.secrets.kv.v2.read_secret(__path,mount_point=self.hcv_mount_point)['data']['data']
        return all_key_list

    def gen_host_keys(self):
        list_of_servers = self.list_servers()
        for srv in list_of_servers:
            _path = f'servers/{srv}/host'
            ssh_host_rsa_key_cmd = shlex.split(f"ssh-keygen -f {_path}/ssh_host_rsa_key -N '' -q -t rsa -C root@{srv.rstrip('/')}")
            ssh_host_ecdsa_key_cmd = shlex.split(f"ssh-keygen -f {_path}/ssh_host_ecdsa_key -N '' -q -t ecdsa -C root@{srv.rstrip('/')}")
            ssh_host_ed25519_key_cmd = shlex.split(f"ssh-keygen -f {_path}/ssh_host_ed25519_key -N '' -q -t ed25519 -C root@{srv.rstrip('/')}")
            if not os.path.exists(_path):
                os.makedirs(_path)
            ssh_host_rsa_key_result = subprocess.run(ssh_host_rsa_key_cmd,stdout=subprocess.PIPE)
            if ssh_host_rsa_key_result:
                print(f'Created host rsa key for {srv}')
            ssh_host_ecdsa_key_result = subprocess.run(ssh_host_ecdsa_key_cmd,stdout=subprocess.PIPE)
            if ssh_host_ecdsa_key_result:
                print(f'Created host ecdsa key for {srv}')
            ssh_host_ed25519_key_result = subprocess.run(ssh_host_ed25519_key_cmd,stdout=subprocess.PIPE)
            if ssh_host_ecdsa_key_result:
                print(f'Created host ed25519 key for {srv}')


    def list_host_keys(self):
        host_keys = defaultdict(list)
        d = {}
        for root, dirs, files in os.walk("servers"):
            for filename in files:
                if filename == 'ssh_host_rsa_key':
                    rsa_pub = 'ssh_host_rsa_key.pub'
                    with open(os.path.join(root,filename), 'r') as _rsa:
                        _ssh_host_rsa_key = _rsa.read()
                    with open(os.path.join(root,rsa_pub), 'r') as _rsa_pub:
                        _ssh_host_rsa_key_pub = _rsa_pub.read()
                    info = {
                            'ssh_host_rsa_key': _ssh_host_rsa_key,
                            'ssh_host_rsa_key_pub': _ssh_host_rsa_key_pub
                            }
                    d.update(info)
                if filename == 'ssh_host_ecdsa_key':
                    ecdsa_pub = 'ssh_host_ecdsa_key.pub'
                    with open(os.path.join(root,filename), 'r') as _ecdsa:
                        _ssh_host_ecdsa_key = _ecdsa.read()
                    with open(os.path.join(root,ecdsa_pub), 'r') as _ecdsa_pub:
                        _ssh_host_ecdsa_key_pub = _ecdsa_pub.read()
                    info = {
                            'ssh_host_ecdsa_key': _ssh_host_ecdsa_key,
                            'ssh_host_ecdsa_key_pub': _ssh_host_ecdsa_key_pub
                            }
                    d.update(info)
                if filename == 'ssh_host_ed25519_key':
                    ed25519_pub = 'ssh_host_ed25519_key.pub'
                    with open(os.path.join(root,filename), 'r') as _ed25519:
                        _ssh_host_ed25519_key = _ed25519.read()
                    with open(os.path.join(root,ed25519_pub), 'r') as _ed25519_pub:
                        _ssh_host_ed25519_key_pub = _ed25519_pub.read()
                    info = {
                            'ssh_host_ed25519_key': _ssh_host_ed25519_key,
                            'ssh_host_ed25519_key_pub': _ssh_host_ed25519_key_pub,
                            }
                    d.update(info)
                host_keys[root].append(d)
        return host_keys


    def upload_host_keys(self):
        for path,keys in self.list_host_keys().items():
            if 'host' not in path:
                continue
            client_response = self.client.secrets.kv.v2.create_or_update_secret(mount_point=self.hcv_mount_point,path=path,secret=dict(keys[0]))

def getargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url',
        action='store',
        dest='hcv_url',
        help='Hashicorp Valt API URL')
    parser.add_argument('--cacert',
        action='store',
        dest='hcv_ca_cert',
        default='/etc/pki/tls/certs/ca-bundle.crt',
        help='CA cert used to connect to the Hashicorp Vault instance')
    parser.add_argument('--secret_mount',
        action='store',
        dest='hcv_mount_point',
        default='credentials',
        help='The path the secret engine is mounted on')
    parser.add_argument('--secret_path',
        action='store',
        dest='hcv_path',
        default='servers',
        help='The path to the secret')
    parser.add_argument('--server',
        action='store',
        dest='srv',
        help='Server Name to get information about.')
    parser.add_argument('--user_key',
        action='store',
        dest='user_key',
        help='The user key to check')
    parser.add_argument('--list-public-keys',
        action='store_true',
        dest='list_public_keys',
        help='List all public keys')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = getargs()
    HCV = ManageVaultSecrets(hcv_url=args.hcv_url,
            hcv_ca_cert=args.hcv_ca_cert,
            hcv_mount_point=args.hcv_mount_point,
            hcv_path=args.hcv_path)
    if args.srv and args.user_key:
        for k,v in HCV.list_key(srv=args.srv, key_name=args.user_key).items():
            print(v)

    if args.list_public_keys:
        for k,v in HCV.list_public_keys().items():
            for rslt in v:
                print(rslt)
