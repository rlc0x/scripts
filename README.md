# scripts

Moving a selection of scripts I have written over the last 10 or so years into this public repository.

## Layout

Each script has its own directory that contains documentation. Most of the scripts will be at the root level of the repository. 

In the event the script was written to work specifically with an operating system (I am looking at you `AIX`) it will reside under a directory representing that operating system. 

## Documentation

Most scripts will contain an `AsciiDoc` document, however I have been porting documentation to LaTex over the last couple of years so there could be a mix. 
