import re
import traceback
import os
import subprocess
from collections import defaultdict
import argparse
from pprint import pprint
import json
import socket

class ShowMount(object):
    def __init__(self, session):
        self.log = logging.getLogger(__name__)
        self.log.addHandler(logging.NullHandler())
        self.session = session

    def __init__(self, nas=None):
        if nas is not None:
            self.nas = socket.gethostbyname(nas)
        else:
            raise ValueError('Name or IP of the nas is required')

    def get_vfs_dirs(self):
        vfs_dirs = []
        vfs_dirs_cmd = 'showmount -e {}'.format(self.nas)
        for line in subprocess.Popen(vfs_dirs_cmd, shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n'):
            line = line.strip().split(' ')
            if re.match(r'/root_vdm.*', line[0]):
                vfs_dirs.append(line[0])
        return vfs_dirs

    def get_nas_clients(self):
        nas_clients = defaultdict(lambda: defaultdict(list))
        nas_clients_cmd = 'showmount -a {}'.format(self.nas)
        re_nas_clients = re.compile('(?P<nfs_client>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(\w+)?([\.|\w]+)):(?P<vfs_client>(.\w+?([\/|\w]+)))')
        for line in subprocess.Popen(nas_clients_cmd, shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n'):
            try:
                re_nas_clients_match = re_nas_clients.search(line)
                if re_nas_clients_match:
                    client_name = re_nas_clients_match.group('nfs_client')
                    vfs = re_nas_clients_match.group('vfs_client')
                if re.match(r'/root_vdm.*', vfs):
                    nas_clients[self.nas][vfs].append(client_name)
                elif re.match(r'/(\w+).*',vfs):
                    nas_clients[self.nas][vfs].append(client_name)
            except:
                pass
        return nas_clients

def getargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--nas',
                        action='store',
                        required=True,
                        dest='nas',
                        help='Name of the NAS to querry')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = getargs()
    SM = ShowMount(nas=args.nas)
    nas_client_list = SM.get_nas_clients()
    if nas_client_list:
        print(json.dumps(nas_client_list, indent=4))
