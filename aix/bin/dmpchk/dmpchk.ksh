#!/usr/bin/ksh93
FPATH=/usr/local/adm/lib
export FPATH
################################################################
function usagemsg_dmpchk {
  print "
Program: dmpchk

This script checks to make sure the system dump device is large enough 
to contain a system dump.

Usage: ${1##*/} [-?vV] 

  Where:
    -v = Verbose mode - displays dmpchk function info
    -V = Very Verbose Mode - debug output displayed
    -s = Silent - Return 0 if no additional PP neded
    -? = Help - display this message

Author: Raymond L Cox (rcox@unixandmore.com)
"
}
configure_dmpchk()
{
#### 
#### Notice this function is a POSIX function so that it can see local
#### and global variables from calling functions and scripts.
#### 
#### Configuration parameters can be stored in a file and
#### this script can be dynamically reconfigured by sending
#### the running script a HUP signal using the kill command.
#### 
#### Configuration variables can be defined in the configuration file using
#### the same syntax as defining a shell variable, e.g.: VARIABLE="value"

 CFILE=/usr/local/adm/etc/dmpchk.conf

  if [[ -f ${CFILE} ]]
  then
      (( VERBOSE == TRUE )) && cat ${CFILE}
      . ${CFILE}
  fi

  return 0
}  
################################################################
function dmpchk {
  typeset VERSION="1.0"
  typeset TRUE="0"
  typeset FALSE="1"
  typeset VERBOSE="${FALSE}"
  typeset VERYVERB="${FALSE}"
  typeset SILENT="${FALSE}"
  typeset EUID=$(id -u)
  typeset ADD_PP_NUM=0

### Add a check to make sure root is running the command, exit if not.

  if [[ "${EUID}" -ne "0" ]]
  then
      print -u 2 "Must be root to run this command"
      return 1
  fi
  typeset DUMPLV="${DUMPLV:-lg_dumplv}"

#### Set up a trap of the HUP signal to cause this script
#### to dynamically configure or reconfigure itself upon
#### receipt of the HUP signal.

  trap "configure_dmpchk ${0}" HUP

#### Read the configuration file and initialize variables by
#### sending this script a HUP signal

  kill -HUP ${$}

#### Process the command line options and arguments.

  while getopts ":vVs" OPTION
  do
      case "${OPTION}" in
          'v') VERBOSE="${TRUE}";;
          'V') VERYVERB="${TRUE}";;
	  's') SILENT="${TRUE}";;
        [?:#]) usagemsg_dmpchk && return 1 ;; 
      esac
  done
   
  shift $(( ${OPTIND} - 1 ))
  
  trap "usagemsg_dmpchk ${0}" EXIT


  trap "-" EXIT
  
  (( VERYVERB == TRUE )) && set -x
  (( VERBOSE  == TRUE )) && print -u 2 "# Version...........: ${VERSION}"


################################################################

  typeset EstimatedSize=$(sysdumpdev -e | awk '{print $NF}')
  (( EstimatedMB = EstimatedSize/1024/1024 ))

  LVINFO=( $(lvinfo -n ${DUMPLV}) )
  eval $(for IDX in "${LVINFO[@]}"
    do
      print "${IDX}"
    done)
  (( NeededPP = EstimatedMB/lv_vg_pp_size ))


  (( VERBOSE == TRUE )) && print -u 2 "DUMPLV = ${DUMPLV}"
  (( VERBOSE == TRUE )) && print -u 2 "EstimatedSize = ${EstimatedSize}"
  (( VERBOSE == TRUE )) && print -u 2 "EstimatedMB = ${EstimatedMB}"
  (( VERBOSE == TRUE )) && print -u 2 "PP_SIZE = ${lv_vg_pp_size}"
  (( VERBOSE == TRUE )) && print -u 2 "NUM_PP = ${lv_pp}"
  (( VERBOSE == TRUE )) && print -u 2 "NeededPP = ${NeededPP}"



  if [[ "${NeededPP}" -gt "${lv_pp}" ]]
  then
	  (( ADD_PP_NUM = NeededPP - NUM_PP ))
  else
	  ADD_PP_NUM=0
  fi
  (( VERBOSE == TRUE )) && print -u 2 "ADD_PP_NUM = ${ADD_PP_NUM}"
  (( SILENT == TRUE )) && return 0
  print -- "add_pp_num="${ADD_PP_NUM}""




  trap "-" HUP

  return 0
}
################################################################

dmpchk "${@}"



