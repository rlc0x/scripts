Name:   PACKAGE
Version:    VERSION
Release:    RELEASE
Summary: Various AIX utilities
Packager: Raymond L Cox
Group:  Application/Other
License:    MIT
URL:    https://gitlab.com/rlc0x/scripts/aix 
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
Autoreq: 0

%description
Various AIX utility scripts

%prep
%setup -q
%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/local/adm/bin
install -m 755 dmpchk/dmpchk.ksh $RPM_BUILD_ROOT/usr/local/adm/bin/dmpchk
install -m 755 lvinfo/lvinfo.ksh $RPM_BUILD_ROOT/usr/local/adm/bin/lvinfo
install -m 755 user_mgr/user_mgr $RPM_BUILD_ROOT/usr/local/adm/bin/user_mgr
install -m 755 audit_mgr/audit_mgr $RPM_BUILD_ROOT/usr/local/adm/bin/audit_mgr
mkdir -p $RPM_BUILD_ROOT/usr/local/adm/lib
install -m 755 lvinfo/lvinfo $RPM_BUILD_ROOT/usr/local/adm/lib/lvinfo
install -m 755 lvinfo/usagemsg_lvinfo $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_lvinfo
install -m 755 create_snap/create_snap $RPM_BUILD_ROOT/usr/local/adm/lib/create_snap
install -m 755 create_snap/usagemsg_create_snap $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_create_snap
install -m 755 ismounted/ismounted $RPM_BUILD_ROOT/usr/local/adm/lib/ismounted
install -m 755 ismounted/usagemsg_ismounted $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_ismounted
install -m 755 list_snap/list_snap $RPM_BUILD_ROOT/usr/local/adm/lib/list_snap
install -m 755 list_snap/usagemsg_list_snap $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_list_snap
install -m 755 mount_snap/mount_snap $RPM_BUILD_ROOT/usr/local/adm/lib/mount_snap
install -m 755 mount_snap/usagemsg_mount_snap $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_mount_snap
install -m 755 remove_snap/remove_snap $RPM_BUILD_ROOT/usr/local/adm/lib/remove_snap
install -m 755 remove_snap/usagemsg_remove_snap $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_remove_snap
install -m 755 vginfo/vginfo $RPM_BUILD_ROOT/usr/local/adm/lib/vginfo
install -m 755 vginfo/usagemsg_vginfo $RPM_BUILD_ROOT/usr/local/adm/lib/usagemsg_vginfo

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,system,-)
/usr/local/adm/bin/lvinfo
/usr/local/adm/bin/dmpchk
/usr/local/adm/bin/user_mgr
/usr/local/adm/bin/audit_mgr
/usr/local/adm/lib/lvinfo
/usr/local/adm/lib/usagemsg_lvinfo
/usr/local/adm/lib/create_snap
/usr/local/adm/lib/usagemsg_create_snap
/usr/local/adm/lib/ismounted
/usr/local/adm/lib/usagemsg_ismounted
/usr/local/adm/lib/list_snap
/usr/local/adm/lib/usagemsg_list_snap
/usr/local/adm/lib/mount_snap
/usr/local/adm/lib/usagemsg_mount_snap
/usr/local/adm/lib/remove_snap
/usr/local/adm/lib/usagemsg_remove_snap
/usr/local/adm/lib/vginfo
/usr/local/adm/lib/usagemsg_vginfo

%doc

%changelog
* Fri Jan 20 2023 Raymond L Cox
- release 1.0.8-1 - Moving into a public repostory
* Thu Dec 23 2021 Raymond L Cox
- release 1.0.7-1 - Added the audit_mgr script to manage the /audit files.
* Thu Dec 02 2021 Raymond L Cox
- release 1.0.6-1 - Updated the dmpchk script to include the verbose and silent flag.
- release 1.0.5-1 - Merging aixlibs into aixutils.
* Fri Nov 05 2021 Raymond L Cox
- release 1.0.4-1 - Added dmpchk utility to help in keeping system dumps up to date
- release 1.0.4-1 - Removed the pre and post backup scripts, moving into dedicated rpm.
* Sat Feb 20 2021 Raymond L Cox
- release 1.0.3-1 - Added -s flag to pre_backup to allow for variable size snapshots.
* Thu Feb 18 2021 Raymond L Cox
- release 1.0.2-1 - Added the lvinfo wrapper. Useful for checking the required space for snapshots.
* Mon Feb 01 2021 Raymond L Cox
- release 1.0.1-2 - Updated documentation in post_backup
* Fri Jan 29 2021 Raymond L Cox
- release 1.0.1 - Add user_mgr Perl script to the package.
* Fri Jan 29 2021 Raymond L Cox
- release 1.0.0 - Intial Release
