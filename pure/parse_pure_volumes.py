#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import config as cfg
from collections import defaultdict
from collections import namedtuple
import json
from pprint import pprint
from purestorage import FlashArray
#import argparse
from optparse import OptionParser

VERSION = '1.0.0'
DEBUG_LEVEL = 0
VERBOSE_FLAG = 0

"""
Used to manage RDM storage on Vmware Linux clients

"""
class PureManager(object):

    def __init__(self, **kwargs):
        """Gather command line entries and set defaults
        Args:
            pure_host (str): Ip address or host name of the Pure FlashArray
            pure_token (str): Access token generated in teh Pure admin interface
            pure_naa: (str): Network Addressing Authority ID associated with Pure flash arrays
            ssl_cert: (str): Location of the ssl certificate used to access the Pure API
         """
        allowed_params = ['pure_user', 'pure_naa', 'pure_token', 'pure_host', 'ssl_cert' ]
        params = locals()
        for key, val in params['kwargs'].items():
            if key not in allowed_params:
                raise TypeError(
                        f"Got an unexpected keywork argument {key} to method __init__"
                        )
            params[key] = val
        del params['kwargs']

        if ('pure_host' not in params) or (params['pure_host'] is None):
            raise ValueError(f"Missing the required parameter `pure_host` when calling `__init__`")
        else:
            self.pure_host = params['pure_host']

        if ('pure_token' not in params) or (params['pure_token'] is None):
            self.pure_token = cfg.pure_array[self.pure_host]
        else:
            self.pure_token = params['pure_token']

        if ('pure_naa' not in params) or (params['pure_naa'] is None):
            self.pure_naa = '624a9370'
        else:
            self.pure_naa = params['pure_naa']

        if ('ssl_cert' not in params) or (params['ssl_cert'] is None):
            self.ssl_cert = '/etc/pki/tls/certs/ca-bundle.crt'
        else:
            self.ssl_cert = params['ssl_cert']

        self.array = FlashArray(self.pure_host, api_token=self.pure_token, verify_https=True, ssl_cert=self.ssl_cert)

    def get_sname(self,s_name=None,ss_name=None):
        if s_name is None and ss_name is None:
            s_name = "contains(name,'vg-epicecp') or contains(name,'vg-epicodb')"
        elif s_name is None and ss_name:
            s_name = f"contains(name, '{ss_name}')"
        elif s_name and ss_name:
            s_name = f"contains(name, '{s_name}') and contains(name, '{ss_name}')"
        return s_name



    def list_volumes(self,s_name=None,ss_name=None,snap=False):
        s_name = self.get_sname(s_name=s_name,ss_name=ss_name)
        volume_list = self.array.list_volumes(filter=s_name,snap=snap)
        if DEBUG_LEVEL == 2:
            pprint(volume_list)
        else:
            return(volume_list)

    def list_hgroups(self,hgroup=None):
        if hgroup is None:
            raise ValueError("The name of a Host Group must be passed to this method")
        else:
            hgroup_list = self.array.list_hgroup_connections(hgroup=hgroup)
        if DEBUG_LEVEL == 2:
            pprint(hgroup_list)
        else:
            return(hgroup_list)

    @property
    def list_vgroups(self):
        return self.array.list_vgroups()

    def list_snapshots(self,s_name=None,hgroup=None,ss_name=None):
        if s_name is None and ss_name:
            filter = f"contains(name, '{ss_name}')"
        elif s_name and ss_name is None:
            filter = f"contains(name, '{s_name}')"
        elif s_name and ss_name:
            filter = f"contains(name, '{s_name}') and contains(name, '{ss_name}')"
        else:
            filter = f"contains(name, 'pg-')"
        snap_vol_list = self.array.list_volumes(filter=filter,snap=True)
        _snap_info = []
        for snap_vol in snap_vol_list:
            try:
                name = snap_vol.get('name')
                serial = snap_vol.get('serial')
                size = snap_vol.get('size')
                created = snap_vol.get('created')
                source = snap_vol.get('source')
                info = {'source_vol': f"{name}",
                        'dest_vol': f"{name.split('/')[1]}",
                        'wwn': f"{serial}",
                        'size_gb': int(size)/float(1<<30),
                        'array': f"{self.pure_host}",
                        'created': f"{created}"
                        }
                _snap_info.append(info)
            except Exception as e:
                pass
        keys = {key for dict_ in _snap_info for key in dict_.keys()}
        SNAP_MAPS = namedtuple('SNAP_MAPS', keys)
        SNAP_MAPS.__new__.__defaults__ = (None,) * len(SNAP_MAPS._fields)
        ret = [SNAP_MAPS(**dict_) for dict_ in _snap_info]
        return(ret)

    def copy_volume(self,src_vol=None,dst_vol=None):
        """Method to copy a volume
        """
        if (src_vol or dst_vol) is None:
            raise ValueError('A required parameter is missing')
        try:
            copy_volume_result = self.array.copy_volume(src_vol,dst_vol)
            print(f"Snapshot Volume: {src_vol} has been copied to {dst_vol}")
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"volume copy failed for: {src_vol} Mesage: {y[0]['msg']}")

    def map_volume(self,pure_host_group=None,vol=None):
        """Method to map the volume to a host_group
        """
        if (vol or pure_host_group) is None:
            raise ValueError('A required parameter is missing')
        try:
            result = self.array.connect_hgroup(pure_host_group,vol)
            print(f"Volume: {vol} has been mapped to: {pure_host_group}")
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"volume mapping failed for: {vol} Mesage: {y[0]['msg']}")
            pass

    def copy_volumes_by_volume_list(self,vlist=None,pure_host_group=None):
        """Method to copy snapshot volumes
        """
        if (vlist or pure_host_group) is None:
            raise AttributeError('A required parameter is missing')
        with open(vlist, mode='r') as vol_file:
            vol_reader = csv.reader(vol_file, delimiter=',')
            for vol in vol_reader:
                (src_vol,dst_vol) = vol[0],vol[1]
                self.copy_volume(src_vol,dst_vol)
                self.map_volume(pure_host_group=pure_host_group,vol=dst_vol)
                #print(f"Would call copy_volume({src_vol},{dst_vol})")
                #print(f"Would call map_volume({dst_vol},{pure_host_group})")

    def list_info(self,s_name=None,hgroup=None):
        vol_list = self.list_volumes(s_name=s_name)
        hg_list = self.list_hgroups(hgroup=hgroup)
        _info = []
        for vol_info in vol_list:
            for hg_info in hg_list:
                name = vol_info.get('name')
                hg_vol = hg_info.get('vol')
                if name == hg_vol:
                    try:
                        (hg_vg, vol) = name.split('/')
                        if vol.startswith('ss'):
                            (_snap, lun_type, dc_loc, sys_name, vg, seq) = vol.split('-')
                        else:
                            (lun_type, dc_loc, sys_name, vg, seq) = vol.split('-')
                        if vol_info.get('source') is not None:
                            snap = True
                        else:
                            snap = False
                        lun = hg_info.get('lun')
                        info = {'lun_type': f'{lun_type}',
                                'dc_loc': f'{dc_loc}',
                                'sys_name': f'{sys_name}',
                                'size_gb': int(vol_info.get('size') / float(1<<30)),
                                'size_tb': int(vol_info.get('size') / float(1<<40)),
                                'wwn': f"{vol_info.get('serial')}",
                                'lun': f'{lun}',
                                'created': f"{vol_info.get('created')}",
                                'snapshot': f"{snap}",
                                'pure_vg': f"{hg_vg}",
                                'pure_host_group': f"{hgroup}",
                                'source': f"{vol_info.get('source')}",
                                'pure_host': f"{self.pure_host}",
                                'vol_name': f'{vol}',
                                'vg': f'{vg}',
                                'seq': f'{seq}',
                        }
                        _info.append(info)
                    except Exception as e:
                        pass
        keys = {key for dict_ in _info for key in dict_.keys()}
        VOLUME_INFO = namedtuple('VOLUME_INFO', keys)
        VOLUME_INFO.__new__.__defaults__ = (None,) * len(VOLUME_INFO._fields)
        ret = [VOLUME_INFO(**dict_) for dict_ in _info]
        return(ret)






def get_args():
    usage = 'usage: %prog [options]'
    version = '%prog ' + VERSION
    description = "Python CLI for Pure Array Management"

    parser = OptionParser(usage=usage, version=version, description=description)
    #parser = argparse.ArgumentParser()
    parser.add_option('--debug', type='int', dest='DEBUG_LEVEL', default=0, help='Debug level for testing')
    parser.add_option('--pure_host', action='store', dest='pure_host', help='The URL or IP of the Pure API')
    parser.add_option('--pure_host_group', action='store', dest='pure_host_group', help='The name of the Pure Host Group for volume mapping')
    parser.add_option('--pure_token', action='store', dest='pure_token', help='The Access Token created in the Pure API')
    parser.add_option('--list_snapshots', action='store_true', dest='list_snapshots', help='When combined with --snapshot_name ans --sys_name wil display all snapshots with that name on that system, otherwise all snapshots')
    parser.add_option('--ss_name', action='store', dest='ss_name', help='Full or partial name of snapshot to search for')
    parser.add_option('--dest_sys', action='store', dest='dest_sys', help='The Destination System shortname for a snapshot')
    parser.add_option('--dest_vg', action='store', dest='dest_vg', help='VG destination name for cloneed volumes')
    parser.add_option('--sys_name', action='store', dest='s_name', help='The name of the system to list RDMs for')
    parser.add_option('--list_volumes', action='store_true', dest='list_volumes', help='List all volumes')
    parser.add_option('--list_host_groups', action='store_true', dest='list_host_groups', default=False, help='List volumes connected to a host_group. This is the easiest way to find LUN information')
    parser.add_option('--host_group', action='store', type='string', dest='hgroup', help='The name of the Host Group to query')
    parser.add_option('--list_info', action='store_true', dest='list_info', default=False, help='List volumes, luns, host_groups, etc...')
    parser.add_option('--generate_rdm_maps', action='store_true', dest='generate_rdm_maps', default=False, help='Display the values to use with an Ansible playbook using community.vmware_guest_disks')
    parser.add_option('--generate_snap_copy_list', action='store_true', dest='generate_snap_copy_list', default=False, help='Create a CSV file that can be used to copy snapshots to volumes')
    parser.add_option('--out_file', action='store', dest='out_file', help='Name of the output file to create')
    parser.add_option('--in_file', action='store', dest='in_file', help='Name of the input file to use')
    parser.add_option('--copy_snaps', action='store_true', dest='copy_snaps', default=False, help='Copy snapshots to volumes from an input list')

    (options, args) = parser.parse_args()

    if (options.list_host_groups or options.list_info or options.generate_rdm_maps) and not options.hgroup:
        parser.error('option --host_group is required when using this command')
    if options.generate_snap_copy_list and not options.out_file:
        parser.error('option --out_file is required when using the --generate_snap_copy_list option')
    if options.copy_snaps and not (options.in_file and options.pure_host_group):
        parser.error('options --in_file and --pure_host_group are required when using the --copy_snaps option')
    return options


if __name__ == '__main__':
    args = get_args()
    DEBUG_LEVEL = args.DEBUG_LEVEL
    pure = PureManager(pure_host=args.pure_host)
    if (args.list_snapshots and args.ss_name):
        pprint(pure.list_snapshots(ss_name=args.ss_name))
    elif (args.list_snapshots and args.s_name):
        pprint(pure.list_snapshots(s_name=s_name))
    elif (args.list_snapshots and args.ss_name and args.s_name):
        pprint(pure.list_snapshots(s_name=s_name,ss_name=args.ss_name))
    elif args.list_snapshots:
        pprint(pure.list_snapshots())
    if args.generate_snap_copy_list:
        snap_list = pure.list_snapshots(ss_name=args.ss_name)
        for snap in snap_list:
            print(f"{snap.source_vol},{snap.dest_vol}")

    if args.copy_snaps:
        pure.copy_volumes_by_volume_list(vlist=args.in_file,pure_host_group=args.pure_host_group)

    if args.list_info:
        volume_info = pure.list_info(hgroup=args.hgroup)
        for k in volume_info:
            print(f"System_name: {k.dc_loc}-{k.sys_name} Volume Name: {k.pure_vg}/{k.vol_name} LUN: {k.lun} WWN: {k.wwn} Size: {k.size_gb} GB Array: {k.pure_host}")
    if args.generate_rdm_maps:
        rdm_maps = pure.list_info(hgroup=args.hgroup)
        for rdm_map in rdm_maps:
            print(f"- {{ type: {rdm_map.lun_type}, state: present, scsi_controller: '', unit_number: '', rdm_path: '/vmfs/devices/disks/naa.624a9370{rdm_map.wwn.lower()}', compatibility_mode: 'physicalMode', disk_mode: 'persistent' }}# {rdm_map.vg}-{rdm_map.seq} LUN {rdm_map.lun}")
        for vm_storage in rdm_maps:
            print(f"- '/dev/disk/by-id/wwn-0x{vm_storage.wwn.lower()}' # {vm_storage.vg}-{vm_storage.seq} LUN {vm_storage.lun}")
    if args.list_volumes:
        volume_list = pure.list_info(hgroup=args.hgroup)
        for k in volume_list:
            print(f"{k.vol_name},{k.size_tb}TB")
    if args.list_host_groups:
        pure.list_hgroups(hgroup=args.hgroup)
