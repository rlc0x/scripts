# -*- coding: utf-8 -*-
import csv
from collections import defaultdict
from collections import namedtuple
import json
from pprint import pprint
from purestorage import FlashArray, PureHTTPError

import argparse


"""
Used to manage RDM storage on Vmware Linux clients
"""
class PureManager(object):

    def __init__(self, **kwargs):
        """Gather command line entries and set defaults
        Args:
            pure_host (str): Ip address or host name of the Pure FlashArray
            pure_token (str): Access token generated in teh Pure admin interface
            ssl_cert: (str): Location of the ssl certificate used to access the Pure API
            host_group (str): Host Group
         """
        allowed_params = ['host_group', 'pure_token', 'pure_host', 'ssl_cert' ]
        params = locals()
        for key, val in params['kwargs'].items():
            if key not in allowed_params:
                raise TypeError(
                        f"Got an unexpected keywork argument {key} to method __init__"
                        )
            params[key] = val
        del params['kwargs']

        if ('pure_host' not in params) or (params['pure_host'] is None):
            raise ValueError(f"Missing the required parameter `pure_host` when calling `__init__`")
        else:
            self.pure_host = params['pure_host']

        if ('pure_token' not in params) or (params['pure_token'] is None):
            raise ValueError("Missing the required parameter `pure_token` when calling `__init__`")
        else:
            self.pure_token = params['pure_token']

        if ('ssl_cert' not in params) or (params['ssl_cert'] is None):
            self.ssl_cert = '/etc/pki/tls/certs/ca-bundle.crt'
        else:
            self.ssl_cert = params['ssl_cert']

        if ('host_group' not in params) or (params['host_group'] is None):
            raise ValueError("Missing the required parameter `host_group` when calling `__init__`")
        else:
            self.host_group = params['host_group']

        self.array = FlashArray(self.pure_host, api_token=self.pure_token, verify_https=True, ssl_cert=self.ssl_cert)

    def copy_volume(self,src_vol,dst_vol):
        """Method to copy a volume
        """
        try:
            copy_volume_result = self.array.copy_volume(src_vol,dst_vol)
            print(f"Snapshot Volume: {src_vol} has been copied to {dst_vol}")
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"volume copy failed for: {src_vol} Mesage: {y[0]['msg']}")

    def create_volume(self,vol,sz):
        """Method to create a volume
        """
        try:
            result = self.array.create_volume(vol,sz)
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"volume creation failed for: {vol} Mesage: {y[0]['msg']}")
            pass

    def extend_volume(self,vol,sz):
        """Resize a volume
        """
        try:
            resized_result = self.array.extend_volume(vol,sz)
            resized_size = int(resized_result.get('size')) / float(1<<40)
            print(f"Volume: {vol} is now: {resized_size}T")
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"Extending the volume: {vol} failed, Message: {y[0]['msg']}")


    def map_volume(self,hgroup,vol):
        """Method to map the volume to a host_group
        """
        try:
            result = self.array.connect_hgroup(hgroup,vol)
            print(f"Volume: {vol} has been mapped to: {hgroup}")
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"volume mapping failed for: {vol} Mesage: {y[0]['msg']}")
            pass

    def unmap_volume(self,vol,host_group):
        try:
            result = self.array.disconnect_hgroup(vol,host_group)
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"Failed to remove the mapping to the host_group {host_group} for: {vol} Mesage: {y[0]['msg']}")
            pass

    def delete_volume(self,vol):
        try:
            result = self.array.destroy_volume(vol)
        except PureHTTPError as response:
            y=json.loads(response.text)
            print(f"Failed to remove the volume {vol}: Mesage: {y[0]['msg']}")
            pass


    def copy_volumes_by_volume_list(self,vlist=None,host_group=None):
        """Method to copy snapshot volumes
        """
        with open(vlist, mode='r') as vol_file:
            vol_reader = csv.reader(vol_file, delimiter=',')
            for vol in vol_reader:
                (src_vol,dst_vol) = vol[0],vol[1]
                self.copy_volume(src_vol,dst_vol)
                self.map_volume(hgroup=self.host_group,vol=dst_vol)
                #print(f"Would call copy_volume({src_vol},{dst_vol})")
                #print(f"Would call map_volume({dst_vol},{self.host_group})")

    def add_volumes_by_volume_list(self,vlist=None,host_group=None):
        """Method used to consume a csv file of existing RDM disks for later use
        Args:
            None
        Returns:
            Dict:
                name (str): Name of the volume
                size (str): Size in bytes or a text repr like 10T or 20G
        """
        with open(vlist, mode='r') as vol_file:
            vol_reader = csv.reader(vol_file, delimiter=',')
            for vol in vol_reader:
                (vname,size) = vol[0],vol[1]
                self.create_volume(vname,size)
                self.map_volume(vname,self.host_group)

    def extend_volumes_by_volume_list(self,vlist=None):
        """Method used to consume a csv file of existing RDM disks for later use
        Args:
            None
        Returns:
            Dict:
                name (str): Name of the volume
                size (str): Size in bytes or a text repr like 10T or 20G
        """
        if vlist is None:
            raise ValueError("Must provide a list")
        with open(vlist, mode='r') as vol_file:
            vol_reader = csv.reader(vol_file, delimiter=',')
            for vol in vol_reader:
                (vname,size) = vol[0],vol[1]
                self.extend_volume(vname,size)

    def remove_volumes_by_volume_list(self,vlist=None,host_group=None):
        """Method used to consume a csv file of existing RDM disks for later use
        Args:
            None
        Returns:
            Dict:
                name (str): Name of the volume
                size (int): Size in bytes
        """
        with open(vlist, mode='r') as vol_file:
            vol_reader = csv.reader(vol_file, delimiter=',')
            for vol in vol_reader:
                (vname,size) = vol[0],vol[1]
                self.unmap_volume(vname,host_group)
                self.delete_volume(vname)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pure_host', action='store', dest='pure_host', help='The URL or IP of the Pure API')
    parser.add_argument('--pure_token', action='store', dest='pure_token', help='The Access Token created in the Pure API')
    parser.add_argument('--host_group', action='store', dest='host_group', help='Pure Host Group')
    parser.add_argument('--volume_list', action='store', dest='volume_list', help='CSV seperated list of volumes and sizes')
    parser.add_argument('--x', action='store_true', dest='extend_true', help='Extend the volume')
    parser.add_argument('--volume_name', action='store', dest='vol', help='Name of the Volume')
    parser.add_argument('--size', action='store', dest='sz', help='Size to make/extend Volume in bytes or using text repr eg: 11G, 11T')
    parser.add_argument('--do_snapshot', action='store_true', help='Create a snapshot from a list')
    parser.add_argument('--copy_snapshot_volumes', action='store_true', help='Copy snapshot volumes from a list into a storage group')
    args = parser.parse_args()
    return args



if __name__ == '__main__':
    args = get_args()
    pure = PureManager(pure_host=args.pure_host,pure_token=args.pure_token,host_group=args.host_group)
    #pure.copy_volumes_by_volume_list(vlist=args.volume_list)
    if args.copy_snapshot_volumes and args.volume_list and args.host_group:
        pure.copy_volumes_by_volume_list(vlist=args.volume_list)
    if args.extend_true and args.volume_list:
        pure.extend_volumes_by_volume_list(vlist=args.volume_list)
    if args.extend_true and args.vol and args.sz:
        pure.extend_volume(vol=args.vol,sz=args.sz)
    #pure.get_volume_list(vlist=args.volume_list)
    #pure.remove_volumes_by_volume_list(vlist=args.volume_list)
