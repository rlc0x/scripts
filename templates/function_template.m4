################################################################
function `usagemsg_'NAME {
  print "
Program: NAME

Place a brief description ( < 255 chars ) of your shell
function here.

Usage: ${1##*/} [-?vV] 

  Where:
    -v = Verbose mode - displays NAME function info
    -V = Very Verbose Mode - debug output displayed
    -k = Output as ksh93 array (default)
    -d = Output as colon (:) delimited data
    -? = Help - display this message

`Author: 'AUTHOR` ('EMAIL`)'

"
}
################################################################
#### 
#### Description:
#### 
#### Place a full text description of your shell function here.
#### 
#### Assumptions:
#### 
#### Provide a list of assumptions your shell function makes,
#### with a description of each assumption.
#### 
#### Dependencies:
#### 
#### Provide a list of dependencies your shell function has,
#### with a description of each dependency.
#### 
#### Products:
#### 
#### Provide a list of output your shell function produces,
#### with a description of each product.
#### 
#### Configured Usage:
#### 
#### Describe how your shell function should be used.
#### 
#### Details:
#### 
#### Place nothing here, the details are your shell function.
#### 
################################################################
function NAME {
  typeset VERSION="1.0"
  typeset TRUE="0"
  typeset FALSE="1"
  typeset KORNOUT="${FALSE}"
  typeset DATAOUT="${FALSE}"
  typeset -L1 D=":"
  typeset VERBOSE="${FALSE}"
  typeset VERYVERB="${FALSE}"

#### Process the command line options and arguments.

  while getopts ":vV" OPTION
  do
      case "${OPTION}" in
          'v') VERBOSE="${TRUE}";;
          'V') VERYVERB="${TRUE}";;
          'k') KORNOUT="${TRUE}";;
          'd') DATAOUT="${TRUE}";;
          '?') NAME "${0}" && return 1 ;;
          ':') NAME "${0}" && return 2 ;;
          '#') NAME "${0}" && return 3 ;;
      esac
  done
   
  shift $(( ${OPTIND} - 1 ))
  
  trap "`usagemsg_'NAME ${0}" EXIT

  trap "-" EXIT
  
    if (( KORNOUT == FALSE )) && (( DATAOUT == FALSE ))
      then
        KORNOUT="${TRUE}"
      fi

  (( VERYVERB == TRUE )) && set -x
  (( VERBOSE  == TRUE )) && print -u 2 "# Version...........: ${VERSION}"


################################################################

####
#### Your shell function should perform it's specfic work here.
#### All work performed by your shell function should be coded
#### within this section of the function.  This does not mean that
#### your function should be called from here, it means the shell
#### code that performs the work of your function should be 
#### incorporated into the body of this function.  This should
#### become your function.
#### 


  trap "-" HUP

  return 0
}
################################################################

NAME "${@}"



