#  -*- coding: utf-8 -*-
# vim: set ts=4 sw=4 ai nu sts=0 nohlsearch

from collections import namedtuple
from optparse import OptionParser
from pprint import pprint
import json
import os
import re
from subprocess import check_output, CalledProcessError
import shlex

__version__ = "0.0.1"
__author__ = "'AUTHOR`"

"""
This template provides a number of the most common examples and will be expanded as needed

Shows the process doing input validation both as a command line script and a library file.

Displays a complex regex and commenting method.

Provides a good example of using the OptionParser to validate input and provide a nice CLI interface

Using namedtuples can really help when creating small functions. The generate_fsinfo example illustrates this.

"""

def generate_fsinfo(lv_name=None):
    """Return information about a single file system

    This is a working function that demonstrates multiple best practices:

    1. kv arguments should be set to None and tested to make sure something is being passed.
    2. Complex regular expressions should use the re.X modifier to allow explaining the logic across multiple lines
    3. Make use of namedtuples when data needs to be returned to another process for evaluation.


    The function looks for the lv_name in the first field of /etc/fstab. As there
    should only be one lv_name mounted at a time, it should only return one file system value

    :param: lv_name: The logical volume only no VG portion
    :type lv_name: str

    :return: Information about the filesystem
    :type return: namedtuple
    """
    if lv_name is None:
        raise ValueError("Missing the `lv_name` value")
    fstab = "/etc/fstab"
    fstab_re = re.compile(
        r"""^  # Match the beginning of the line
                            (?P<fs_spec>[/|\w|-]+)  # Match the LV and capture into a group called fs_spec
                            \s+                     # Match one or more spaces
                            (?P<fs_file>[/|\w]+)    # Match the mount point and capture into a group called fs_file
                            \s+                     # Match one or more spaces
                            (?P<fs_vfstype>\w+)     # Match the fstype and capture into a group called fs_vfstype
                            \s+                     # Match one or more spaces
                            (?P<fs_mntops>[\w|,]+)  # Match mount options,  could be something like rw,noatime,... and capture into a group called fs_mntops
                            \s+                     # Match one or more spaces
                            (?P<fs_freq>\d)         # Match a single digit and capture into a group called fs_freq
                            \s                      # Match a single space
                            (?P<fs_passno>\d)       # Match a single digitn and capture into a group called fs_passno
                            """,
        re.X,
    ).search
    with open(fstab, "r") as f:
        fsinfo_lst = []
        for line in f:
            match = fstab_re(line)
            if match and (lv_name in match.group("fs_spec")):
                info = {
                    "fs_spec": match.group("fs_spec"),
                    "fs_file": match.group("fs_file"),
                    "fs_vfstype": match.group("fs_vfstype"),
                    "fs_mntops": match.group("fs_mntops"),
                    "fs_freq": match.group("fs_freq"),
                    "fs_passno": match.group("fs_passno"),
                }
                fsinfo_lst.append(info)
        if len(fsinfo_lst) > 0:
            keys = {key for dict_ in fsinfo_lst for key in dict_.keys()}
            FSINFO = namedtuple("FSINFO", keys)
            FSINFO.__new__.__defaults__ = (None,) * len(FSINFO._fields)
            fsinfo = [FSINFO(**dict_) for dict_ in fsinfo_lst]
            return fsinfo[0]
        else:
            return None


def example_function(sample_flag1=None,do_something=False):
    """Example function using the subprocess module
    The function should check the value of any required flag or positional argument.


    Three methods using the subprocess method to run a command and process the output
    """

    if sample_flag1 is None:
        raise ValueError('missing required value')
    if sample_flag1 and not do_something:
        raise AttributeError("Provided a flag but not asked to do anything")


    #Use shlex to split any command lines before calling subprocess
    sample_cmd = shlex.split(f"/usr/bin/sample_cmd --do_something={do_something}")
    try:
        check_output(
                sample_cmd,
                stderr=subprocess.STDOUT,
                text=True,
        )
        return True
    except CalledProcessError as response:
        y = json.loads(response.output)
        print(f"Failed to run the sample_command. The error was: {y}")
        raise

    try:
        sample_cmd_result = (
        subprocess.run(
            sample_cmd, stdout=subprocess.PIPE
        )
        .stdout.decode("utf-8")
        .split("\n")
        )
    except CalledProcessError as response:
        print(f"Failed to run the sample_command. The error was {str(response)}")
        raise

    sample_cmd_result = subprocess.run(sample_cmd, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
    if sample_cmd_result:
        # The command succeeded now do something with the results.


def get_args():
    usage = "usage: %prog [options]"
    version = "%prog " + __version__
    description = "'DESCRIPTION"
    parser = OptionParser(usage=usage, version=version, description=description)
    parser.add_option(
        "--sample_flag1", action="store", dest="sample_flag1", help="Sample flag description"
    )
    parser.add_option(
        "--do_something", action="store_true", dest="do_something", default=False, help="Set the do_something to True if selected [default: %default]"
    )
    parser.add_option(
        "--lv_name", action="store", dest="lv_name", help="Name of the logical volume to list filesystem info for"
    )



    (options, args) = parser.parse_args()

    
    # Check for environment variables for some of the options
    """Check for environment variables in the get_args function
    The variable should set flags if defined. 

    Use this space to check for any required flags and combinations
    that will negate eachother or are required together.
    """
    sample_flag1 = os.getenv("SAMPLE_FLAG1")
    do_something = os.getenv("DO_SOMETHING")

    if sample_flag1:
        options.sample_flag1 = sample_flag1
    if do_something:
        options.do_something = do_something

    if (options.do_something and not options.sample_flag1):
        parser.error("Requested to do something, but no data provided to act on (--sample_flag1)")

    #A more complex check. Probably not ever going to run into this.
    if ((options.do_something and not options.sample_flag1) or (options.sample_flag1 and not options.do_something)):
        parser.error("Either asked to do something with no flag or a flag without asking to do something.")
    return options

if __name__ == "__main__":
    args = get_args()
    if (args.sample_flag1 and args.do_something):
        example_function(sample_flag1=args.sample_flag1,do_something=args.do_something)
    if args.lv_name:
        info = generate_fsinfo(lv_name=args.lv_name)
        pprint(info)


